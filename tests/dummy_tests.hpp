#ifndef DUMMYTEST_H
#define DUMMYTEST_H

#include "../src/dummy.hpp"
#include "memorytest.hpp"

class DummyTest : public MemoryTest {};

TEST_F(DummyTest, a_new_object_should_have_no_data) {
  Dummy d;
  ASSERT_FALSE(d.hasData()) << "A new object should not have any data.";
  ASSERT_EQ(d.data(), nullptr) << "A new object should only have a nullpointer";
}

TEST_F(DummyTest, setting_data_should_be_recognised) {
  Dummy d;
  d.data() = new int[4];

  ASSERT_TRUE(d.hasData()) << "A Dummy should not have any data if it was set.";

  delete[] d.data();
}

class parameterized_example
  : public testing::TestWithParam<std::tuple<int, int>> {};

TEST_P(parameterized_example, ChecksIfEqual) {
  int a = std::get<0>(GetParam());
  int b = std::get<1>(GetParam());
  ASSERT_EQ(a, b);
}

INSTANTIATE_TEST_SUITE_P(
  tests,
  parameterized_example,
  ::testing::ValuesIn(std::vector<std::tuple<int, int>>{
    {0, 0}, {1, 1}})
);

#endif  // DUMMYTEST_H
