#include <gtest/gtest.h>

#include "dummy_tests.hpp"
#include "memoryleak_tests.hpp"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
