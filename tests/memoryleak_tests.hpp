#ifndef MEMORYLEAKS_TEST_H
#define MEMORYLEAKS_TEST_H

#include "memoryleaks.hpp"
#include "memorytest.hpp"

TEST(MemoryLeaks_Test, default_object_should_have_no_leaks) {
  const MemoryLeaks leaks_implicit;
  ASSERT_FALSE(leaks_implicit.hasBeenLeaks())
      << "An empty MemoryLeaks object should have no leaks recorded";
}

struct InitTestCase {
  int         leaked            = 0;
  int         dubious           = 0;
  int         reachable         = 0;
  int         suppressed        = 0;
  bool        should_have_leaks = false;
  std::string failure_message;
};

class MemoryLeaks_Initialisation_Test : public testing::TestWithParam<InitTestCase> {};

TEST_P(MemoryLeaks_Initialisation_Test, leak_obkects_should_be_initialised_correctly) {
  auto [
    leaked_,
    dubious_,
    reachable_,
    suppressed_,
    should_have_leaks,
    failure_message
  ] = GetParam();
  const MemoryLeaks leaked(leaked_, dubious_, reachable_, suppressed_);
  ASSERT_EQ(leaked.hasBeenLeaks(), should_have_leaks) << failure_message;
}

const std::vector<InitTestCase> initialisation_test_cases = {
  InitTestCase{
    0,
    0,
    0,
    0,
    false,
    "An empty MemoryLeaks object should have no leaks recorded"
  },
  InitTestCase{
    1,
    0,
    0,
    0,
    true,
    "An obvious leaks should have been recorded"
  },
  InitTestCase{
    0,
    1,
    0,
    0,
    true,
    "An dubious leaks should have been recorded"
  },
  InitTestCase{
    0,
    0,
    1,
    0,
    true,
    "An reachable leaks should have been recorded"
  },
  InitTestCase{
    0,
    0,
    0,
    1,
    true,
    "An supressed leaks should have been recorded"
  }
};

INSTANTIATE_TEST_SUITE_P(
  MemoryLeaks_Test,
  MemoryLeaks_Initialisation_Test,
  ::testing::ValuesIn(initialisation_test_cases)
);

class LeakObjectTests : public testing::Test {
protected:
  LeakObjectTests()
    : without_leaks(0, 0, 0, 0),
      with_leaks(1, 1, 1, 1) {}

  const MemoryLeaks without_leaks;
  const MemoryLeaks with_leaks;
};

TEST_F(LeakObjectTests, adding_two_empty_should_be_an_empty_object) {
  const auto leaks = without_leaks + without_leaks;
  ASSERT_FALSE(leaks.hasBeenLeaks())
      << "Adding two empty objects should result in no leaks.";
}

TEST_F(
  LeakObjectTests,
  subtracting_same_number_of_leaks_should_be_an_empty_object
) {
  const auto leaks = with_leaks - with_leaks;
  ASSERT_FALSE(leaks.hasBeenLeaks())
      << "Subtracting the same objects should result in no leaks.";
}

TEST_F(LeakObjectTests, adding_leaks_should_return_result_in_leaks) {
  const auto leaks = without_leaks + with_leaks;
  ASSERT_TRUE(leaks.hasBeenLeaks()) << "Adding leaks to an empty MemoryLeaks "
                                       "object should have recorded leaks.";
}

TEST_F(
  LeakObjectTests,
  subtracting_fom_empty_objeyt_should_be_an_empty_object
) {
  const auto leaks = without_leaks - with_leaks;
  ASSERT_FALSE(leaks.hasBeenLeaks())
      << "Subtracting the leaks should create no leaks.";
}

#endif  // MEMORYLEAKS_TEST_H
