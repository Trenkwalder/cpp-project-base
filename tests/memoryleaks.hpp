#ifndef MEMORYLEAKS_H
#define MEMORYLEAKS_H

#include <cstddef>

class MemoryLeaks {
public:
    MemoryLeaks();

    MemoryLeaks(
        std::size_t leaked,
        std::size_t dubious,
        std::size_t reachable,
        std::size_t suppressed
    );

    MemoryLeaks(const MemoryLeaks &other) = default;

    MemoryLeaks &operator=(const MemoryLeaks &other) = default;

    MemoryLeaks(MemoryLeaks &&other) = default;

    MemoryLeaks &operator=(MemoryLeaks &&other) = default;

    virtual ~MemoryLeaks() = default;

    bool hasBeenLeaks() const;

    static MemoryLeaks measureLeaks();

    MemoryLeaks operator+(const MemoryLeaks &other) const;

    MemoryLeaks operator-(const MemoryLeaks &other) const;

protected:
private:
    std::size_t _leaked;
    std::size_t _dubious;
    std::size_t _reachable;
    std::size_t _suppressed;

    static std::size_t subtraction_without_underflow(
        std::size_t left,
        std::size_t right
    );

    static std::size_t addition_without_overflow(
        std::size_t left,
        std::size_t right
    );
};

#endif  // MEMORYLEAKS_H
