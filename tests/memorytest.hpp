#ifndef MEMORYTEST_H
#define MEMORYTEST_H

#include <gtest/gtest.h>

#include "memoryleaks.hpp"

class MemoryTest : public ::testing::Test {
 protected:
 private:
  MemoryLeaks _preExistingLeaks;

 public:
  MemoryTest();
  ~MemoryTest() override;

  void SetUp() override;
  void TearDown() override;
};

#endif  // MEMORYTEST_H
