
#include "memoryleaks.hpp"

#include <valgrind/callgrind.h>
#include <valgrind/helgrind.h>
#include <valgrind/memcheck.h>
#include <valgrind/valgrind.h>

#include <algorithm>
#include <cstddef>
#include <limits>

using std::max;
using std::min;
using std::size_t;

MemoryLeaks::MemoryLeaks() : MemoryLeaks(0, 0, 0, 0) {}

MemoryLeaks::MemoryLeaks(
  const size_t leaked,
  const size_t dubious,
  const size_t reachable,
  const size_t suppressed
)
  : _leaked(leaked),
    _dubious(dubious),
    _reachable(reachable),
    _suppressed(suppressed) {}

bool MemoryLeaks::hasBeenLeaks() const {
  return _leaked + _dubious + _reachable + _suppressed; // false if all == 0
}

MemoryLeaks MemoryLeaks::operator+(const MemoryLeaks &other) const {
  return {
    addition_without_overflow(_leaked, other._leaked),
    addition_without_overflow(_dubious, other._dubious),
    addition_without_overflow(_reachable, other._reachable),
    addition_without_overflow(_suppressed, other._suppressed)
  };
}

MemoryLeaks MemoryLeaks::operator-(const MemoryLeaks &other) const {
  return {
    subtraction_without_underflow(_leaked, other._leaked),
    subtraction_without_underflow(_dubious, other._dubious),
    subtraction_without_underflow(_reachable, other._reachable),
    subtraction_without_underflow(_suppressed, other._suppressed)
  };
}

MemoryLeaks MemoryLeaks::measureLeaks() {
  if (RUNNING_ON_VALGRIND) {
    VALGRIND_DO_ADDED_LEAK_CHECK;

    unsigned long leaked, dubious, reachable, suppressed;
    VALGRIND_COUNT_LEAKS(leaked, dubious, reachable, suppressed)

    return MemoryLeaks{leaked, dubious, reachable, suppressed};
  }

  return {};
}

size_t MemoryLeaks::subtraction_without_underflow(
  const size_t left,
  const size_t right
) {
  if (left <= right) {
    return 0;
  }

  return left - right;
}

size_t MemoryLeaks::addition_without_overflow(
  const size_t left,
  const size_t right
) {
  if (right == 0) {
    return left;
  }

  if (left == 0) {
    return right;
  }

  const auto result = left + right;
  if (result > left && result > right) {
    return result;
  }

  return std::numeric_limits<size_t>::max();
}
