#include <iostream>
#include <memory>

#include "src/dummy.hpp"

int main() {
  // Tests for the static analysers:
  //  int *var = new int(5);
  //  auto var = std::make_unique<int>(5);

  std::cout << "Hello World!" << std::endl;

  // delete var;

  return 0;
}
