C++ Project Base
================
Bedges
------

master: [![Build Status](https://gitlab.com/Trenkwalder/cpp-project-base/badges/master/pipeline.svg)](https://gitlab.com/Trenkwalder/cpp-project-base/-/commits/master) 
Develop: [![Build Status](https://gitlab.com/Trenkwalder/cpp-project-base/badges/develop/pipeline.svg)](https://gitlab.com/Trenkwalder/cpp-project-base/-/commits/develop)

[![codecov](https://codecov.io/gl/Trenkwalder/cpp-project-base/branch/develop/graph/badge.svg?token=20GGEKL7IF)](https://codecov.io/gl/Trenkwalder/cpp-project-base) 
[![deepcode](https://www.deepcode.ai/api/gl/badge?key=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwbGF0Zm9ybTEiOiJnbCIsIm93bmVyMSI6IlRyZW5rd2FsZGVyIiwicmVwbzEiOiJjcHAtcHJvamVjdC1iYXNlIiwiaW5jbHVkZUxpbnQiOmZhbHNlLCJhdXRob3JJZCI6Mjk3OTUsImlhdCI6MTYyMDkxMzMzNn0.5-8PN4osVaPeOYhaTM3frQjD63_y3dMgwD-gjPXbL_E)](https://www.deepcode.ai/app/gl/Trenkwalder/cpp-project-base/_/dashboard?utm_content=gl%2FTrenkwalder%2Fcpp-project-base)

Introduction
------------
This repo contains a small C++ project (main.cpp + src/dummy.cpp).
It is setup with **CMAKE** and is used to test the Google Test (GTest environment).
Note that the tests are so designed that each test is checked for memory leaks under the condition that the tests are called with Valgrind.
Finally, a GitLab CI integration is done, which runs the pipeline.

Dependencies
------------

This project depends on:
 - CMAKE (cmake)
 - Google Tests (gtest)
 - GNU Coverage (gcov)
 - Valgrind (valgrind)

 To install these packages in an Debian/Ubuntu Linux, use the following command
```sh
sudo apt install build-essential cmake libgtest-dev gcovr valgrind
```


How to use it?
--------------

Simply run a common CMAKE build with:
```sh
mkdir build/
cd build/
cmake ..
```

Then you can build the project with:
```sh
make all
```
This command builds the main executable, called `build/application`, and the Google Tests, `build/tests/tests`.

You can run the
 - application with `> build/application`
 - tests with `> build/tests/tests`


 To run the tests and check each individual test for memory leaks, simply run
```sh
valgrind --leak-check=full --show-leak-kinds=all --tool=memcheck --log-file="build/tests_valgrind.log" build/tests/tests
```


Where can I find stuff?
-----------------------

This project is organisied in the following way:
 - `./main.cpp` is the root file which starts the applicaiton
 - `src/` and subfolders contain all source files  that are needed to compile the application
 - `tests/` and subfolders contain all tests and a `tests/main.cpp` which starts all tests.
