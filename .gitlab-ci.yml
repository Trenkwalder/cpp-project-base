# This file is a template, and might need editing before it works on your project.
# use the official gcc image, based on debian
# can use verions as well, like gcc:5.2
# see https://hub.docker.com/_/gcc/

stages: 
  - build
  - test
  - run
  - documentation

#####
# Build + Runs
#####
build:gcc:
  image: gcc
  stage: build

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake -y

  script:
    - mkdir build
    - cd build
    - cmake ..
    - cmake --build . --target application --use-stderr -- -j 8
    - cp application ../gcc.applicaiton
    - cmake --build . --target application.lib --use-stderr -- -j 8
    - cp libapplication.lib.so ../gcc.libApplication.so
    - cd ..

  artifacts:
    paths:
      - gcc.applicaiton
      - gcc.libApplication.so

build:clang:
  image: silkeh/clang
  stage: build

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake git -y

  script:
    - mkdir build
    - cd build
    - cmake ..
    - cmake --build . --target application --use-stderr -- -j 8
    - cp application ../clang.applicaiton
    - cmake --build . --target application.lib --use-stderr -- -j 8
    - cp libapplication.lib.so ../clang.libApplication.so
    - cd ..

  artifacts:
    paths:
      - clang.applicaiton
      - clang.libApplication.so



#####
# Unit Tests + Sanitizers
#####

unit:clang:tsan:
  image: silkeh/clang
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake valgrind git -y

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=tsan ..
    - cmake --build . --target tests --use-stderr -- -j 8
    - cp tests/tests ../tsan.tests
    - cd ..
    - ./tsan.tests
    - ./tsan.tests > tsan.results.txt 2> tsan.errors.txt

  artifacts:
    paths:
      - tsan.tests
      - tsan.results.txt
      - tsan.errors.txt

unit:clang:asan:
  image: silkeh/clang
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake valgrind git -y

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=asan ..
    - cmake --build . --target tests --use-stderr -- -j 8
    - cp tests/tests ../asan.tests
    - cd ..
    - ./asan.tests
    - ./asan.tests > asan.results.txt 2> asan.errors.txt

  artifacts:
    paths:
      - asan.tests
      - asan.results.txt
      - asan.errors.txt

unit:clang:lsan:
  image: silkeh/clang
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake valgrind git -y

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=lsan ..
    - cmake --build . --target tests --use-stderr -- -j 8
    - cp tests/tests ../lsan.tests
    - cd ..
    - ./lsan.tests
    - ./lsan.tests > lsan.results.txt 2> lsan.errors.txt

  artifacts:
    paths:
      - lsan.tests
      - lsan.results.txt
      - lsan.errors.txt

unit:clang:msan:
  image: silkeh/clang
  stage: test

  #because of the false positive (libc), I allow it
  allow_failure: true

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake valgrind git -y
    ## Fix Memory-Sanitizer (due to not-properly instrumented libs)
    ## This extends the pipeline for 20+ min. Hence, for this exercise, this stage will fail due to a false positive
    #- apt install cmake valgrind zlib1g zlib1g-dev ninja-build -y
    #- apt install cmake python3 -y
    #- git clone --depth=1 https://github.com/llvm/llvm-project.git
    #- cd llvm-project
    #- cmake -S llvm -B build -G Ninja -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi" -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DLLVM_USE_SANITIZER=MemoryWithOrigins
    #- cmake --build build -- cxx cxxabi -j 8

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=ubsan ..
    - cmake --build . --target tests --use-stderr -- -j 8
    ## This fix allows the use of the newly generated libc++ -> which is compiled with the memory sanitizer.
    #- cmake -DLLVM_USE_SANITIZER=MemoryWithOrigins -DCLANG_DEFAULT_CXX_STDLIB="../llvm-project/build/lib/libc++" ..
    #- cmake -E env LDFLAGS="-rpath=../llvm-project/build/lib/" cmake  --build . --target tests --use-stderr -- -j8
    - cp tests/tests ../msan.tests
    - cd ..
    - ./msan.tests
    - ./msan.tests > msan.results.txt 2> msan.errors.txt

  artifacts:
    paths:
      - msan.tests
      - msan.results.txt
      - msan.errors.txt

unit:clang:ubsan:
  image: silkeh/clang
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake valgrind git -y

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=ubsan ..
    - cmake --build . --target tests --use-stderr -- -j 8
    - cp tests/tests ../ubsan.tests
    - cd ..
    - ./ubsan.tests
    - ./ubsan.tests > ubsan.results.txt 2> ubsan.errors.txt

  artifacts:
    paths:
      - ubsan.tests
      - ubsan.results.txt
      - ubsan.errors.txt

unit:valgrind:
  image: gcc
  stage: test

  before_script: 
    - apt update 
    - apt upgrade -y
    - apt install -y -qq valgrind git cmake
    
  script:
    - mkdir build
    - cd build
    - cmake ..
    - cmake --build . --target tests --use-stderr -- -j 8
    - cp tests/tests ../valgrind_tests
    - cd ..
    - valgrind --leak-check=full --show-leak-kinds=all --tool=memcheck --log-file="tests_valgrind.log" ./valgrind_tests
    
  artifacts:
    paths:
      - tests_valgrind.log

analysis:cppcheck:
  image: ubuntu
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cppcheck git -y

  script:
    - cppcheck --enable=all src/* --xml --xml-version=2 2> static.xml
    - cppcheck-htmlreport --source-dir=. --title=GraphStore --file=static.xml --report-dir=results

  artifacts:
    paths:
      - static.xml
      - results

analysis:clang-tidy:
  image: silkeh/clang
  stage: test

  before_script: 
    - apt update 
    - apt upgrade -y
    - apt-get install -y -qq git clang-tidy
    
  script:
    - clang-tidy -header-filter='.*' -checks='*' src/* -- -std=c++20 > tidy.log 2>&1
    
  artifacts:
    paths:
      - tidy.log

      
coverage:lcov:
  image: gcc
  stage: test

  before_script:
    - apt update
    - apt upgrade -y
    - apt install cmake lcov gcovr git valgrind -y

  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=coverage ..
    - cmake --build . --target tests_coverage --use-stderr -- -j 8
    - cp tests/tests_coverage ..
    - cd ..
    - ./tests_coverage
    - lcov --capture --directory ./ --output-file coverage.info --rc lcov_branch_coverage=1
    - lcov -r coverage.info '/usr/*' 'src' '*gtest*' '**/tests' 'tests/**' 'tests' -o coverage.redacted.info --rc lcov_branch_coverage=1
    - genhtml coverage.redacted.info --output-directory coverage --rc genhtml_branch_coverage=1  --branch-coverage
    - bash <(curl -s https://codecov.io/bash) -t $CODECOVTOKEN -f coverage.info || echo "Codecov did not collect coverage reports"

  artifacts:
    paths:
      - tests_coverage
      - coverage.info
      - coverage.redacted.info
      - coverage


#####
# Doxygen Documentation
#####

documentation:
  image: texlive/texlive
  stage: documentation

  before_script:
    - apt update
    - apt upgrade -y
    - apt install doxygen -y

  script:
    - doxygen Doxyfile

  artifacts:
    paths:
      - docs/
      
